<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('dbconfig.php');
session_start();


function getForm(string $submitName, string $submitValue)
{
    $form = <<<HEREDOC
    <form method="POST">
    <label for="username">User Name : </label>
    <input type="text" name="username" id="username" required>
    <label for="password">Password : </label>
    <input type="password" name="password" id="password" required>
    <input type="submit" name="$submitName" value="$submitValue">
    </form>
HEREDOC;
    return $form;
}


// 



if (count($_POST) > 0) {   // we've been here - either GOOD or BAD login has happened
	echo "<br>checking login";
	if (isset($_POST['submit_login'])) {
		echo "<br>checking pass";
		
		// retrieve user input - you still need to do data validation and sanitizing
		$userName = (isset($_POST['username'])) ? $_POST['username'] : null;
		$passWord = (isset($_POST['password'])) ? $_POST['password'] : null;

		$sql = "(SELECT * FROM mahrss_users WHERE username = :username)";
		$query = $dbConn->prepare($sql);
		$query->bindValue(':username', $userName);
		$query->execute();
		
		$queryResult = $query->fetch(PDO::FETCH_ASSOC);
		
		//var_dump($queryResult);
		$hash = $queryResult['password'];
				
				$verify = password_verify($passWord, $hash); // verify
				if ($verify === true) {
					echo '<br>User Login Validated';
							$_SESSION['is_logged_in'] = true;
							//echo "LOGIN WORKS!";
							header("Location: admin.php");
					
				} else  { 
					echo '<br>Login Not Valid, try again?';
					$loginForm = getForm('submit_login', 'Login');
					echo $loginForm;
		
				}
		
		
	
}

} else {  // first time, present login form
	
$loginForm = getForm('submit_login', 'Login');
echo $loginForm;
	
}


