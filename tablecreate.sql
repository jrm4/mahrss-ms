/* Many mysql using projects would ALSO have you create a new database, eg something like:
CREATE DATABASE mahrss;
but our server doesn't work that way, you have to use the database named after you, and just make tables. Thus the 'mahrss_' naming convention
*/

use jrm4;

CREATE TABLE mahrss_users (
id int(11) NOT NULL auto_increment,
username varchar(255) NOT NULL,
password varchar(255) NOT NULL,
PRIMARY KEY (id)
);
